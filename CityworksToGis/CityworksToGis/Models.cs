﻿namespace CityworksToGis
{
    internal class Employee
    {
        public string EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public decimal Rate { get; set; }
        public string SupervisorName { get; set; }
        public string UnionName { get; set; }
        public string EmploymentCategory { get; set; }
        public string PayrollRoute { get; set; }
        public string SupervisorEmployeeId { get; set; }
        public string EarningsPolicy { get; set; }
        public string EmployeeType { get; set; }
    }

    internal class Project
    {
        public string ProjectNumber { get; set; }
        public string ProjectName { get; set; }
        public string TaskNumber { get; set; }
        public string TaskName { get; set; }
        public string OrgName { get; set; }
    }
}
