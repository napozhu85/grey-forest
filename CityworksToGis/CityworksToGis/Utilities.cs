﻿using Cityworks.Core;
using CityworksToGis.Properties;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace CityworksToGis
{
    internal static class Utilities
    {
        private static readonly CW _db = new CW();
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly StringBuilder _errorMsg = new StringBuilder();
        private static readonly string _token = GetToken();

        internal static void PushToGis()
        {
            Info($"{System.Reflection.MethodBase.GetCurrentMethod().Name} - EXECUTE");

            try
            {
                var start = DateTime.Now.AddSeconds(-305);
                if (_token is null) throw new Exception("Token service/creds not configured.");

                var fieldMappings = new List<Tuple<string, string>>();
                foreach (var mapping in Settings.Default.FieldMappings)
                {
                    var tuple = mapping.Split(new[] { ',' });
                    fieldMappings.Add(new Tuple<string, string>(tuple[0], tuple[1]));
                }

                var woTemplateIds = new[] { Settings.Default.WoTemplateId1, Settings.Default.WoTemplateId2,
                    Settings.Default.WoTemplateId3 };
                using (var client = new WebClient())
                {
                    _db.WORKORDERs.Where(w => w.DATEWOCLOSED >= start && woTemplateIds.Contains(w.WOTEMPLATEID)).ToList()
                        .ForEach(w =>
                        {
                            var entity = _db.WORKORDERENTITies.FirstOrDefault(e => e.WORKORDERSID == w.WORKORDERSID
                                && e.ENTITYSID.HasValue && e.ENTITYSID > 0);
                            if (entity is null)
                            {
                                Log($"Invalid entity for WO# {w.WORKORDERID}");
                                return;
                            }

                            var gisService = "";
                            if (entity.ENTITYTYPE == Settings.Default.EntityType1)
                            {
                                gisService = Settings.Default.GisService1;
                            }
                            else if (entity.ENTITYTYPE == Settings.Default.EntityType2)
                            {
                                gisService = Settings.Default.GisService2;
                            }
                            else if (entity.ENTITYTYPE == Settings.Default.EntityType3)
                            {
                                gisService = Settings.Default.GisService3;
                            }

                            if (gisService == "")
                            {
                                Log($"Invalid entity type for WO# {w.WORKORDERID}");
                                return;
                            }

                            _db.WOCUSTFIELDs.Where(f => f.WORKORDERSID == w.WORKORDERSID).ToList()
                                .ForEach(f =>
                                {
                                    var fieldName = f.CUSTFIELDNAME;
                                    var gisFieldName = fieldMappings.FirstOrDefault(m => m.Item1 == fieldName)?.Item2;
                                    if (gisFieldName != null)
                                    {
                                        var fieldValue = f.CUSTFIELDVALUE;
                                        if (fieldName.EndsWith("Length Installed")
                                            && !string.IsNullOrWhiteSpace(fieldValue))
                                        {
                                            fieldValue = fieldName.Split(new[] { ' ' }).First();
                                            if (fieldValue == "1.25\"") fieldValue = "1 1/4\"";
                                        }
                                        else if (gisFieldName == "TESTDATE")
                                        {
                                            fieldValue = GetEpochTime(w.ACTUALFINISHDATE.Value).ToString();
                                        }
                                        else if (gisFieldName.EndsWith("DATE") && DateTime.TryParse(fieldValue,
                                            out var date))
                                        {
                                            fieldValue = GetEpochTime(date).ToString();
                                        }

                                        UpdateFeature(client, gisService, (int)entity.ENTITYSID, gisFieldName, fieldValue);
                                    }
                                });

                            Info($"WO# {w.WORKORDERID} processed");
                        });
                }
            }
            catch (Exception e)
            {
                Log(e);
            }
        }

        internal static void Info(string message) => _log.Info(message);

        internal static void Log(Exception e)
        {
            _log.Error(e);
            _errorMsg.AppendLine(e.Message);
        }

        internal static void Log(string message)
        {
            _log.Error(message);
            _errorMsg.AppendLine(message);
        }

        internal static void Warn(Exception e)
        {
            _log.Warn(e);
            _errorMsg.AppendLine(e.Message);
        }

        internal static void Warn(string message)
        {
            _log.Warn(message);
            _errorMsg.AppendLine(message);
        }

        private static long GetEpochTime(DateTime date) => date.ToEsriEpochTime();

        private static string GetToken()
        {
            if (!string.IsNullOrWhiteSpace(Settings.Default.TokenService) && !string.IsNullOrWhiteSpace(Settings.Default.GisUser) && !string.IsNullOrWhiteSpace(Settings.Default.GisPassword))
            {
                ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors)
                    => true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11
                    | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                using (var client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    var json = client.UploadString(Settings.Default.TokenService, $"username={Settings.Default.GisUser}&password={Settings.Default.GisPassword}&ip=&client=referer&referer=https%3A%2F%2F{Settings.Default.PortalDomain}%2Fserver%2Fadmin&expiration=60&f=pjson");
                    var jObj = JObject.Parse(json);
                    return jObj["token"].ToString();
                }
            }
            else return null;
        }

        private static void UpdateFeature(WebClient client, string gisService, int objectId, string fieldName,
            string fieldValue)
        {
            client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
            var parameters = $"f=pjson&features=[{{\"attributes\":{{\"objectid\":{objectId},\"{fieldName}\":{fieldValue}}}}}]&token={_token}";
            Info(gisService);
            Info(parameters);
            Info(client.UploadString($"{gisService}/updateFeatures", parameters));
        }
    }
}