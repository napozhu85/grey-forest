﻿using System;

namespace CityworksToGis
{
    internal class Program
    {
        private static readonly string Caption = "Cityworks/GIS Interface";

        private static void Main(string[] args)
        {
            Utilities.Info($"{Caption} - START");

            try
            {
                Utilities.PushToGis();
            }
            catch (Exception e)
            {
                Utilities.Log(e);
            }

            Utilities.Info($"{Caption} - FINISH");
        }
    }
}