//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AVRInterface
{
    using System;
    using System.Collections.Generic;
    
    public partial class CATEGORYCUSTFIELD
    {
        public decimal CUSTFIELDID { get; set; }
        public decimal CATEGORYID { get; set; }
        public string CUSTFIELDNAME { get; set; }
        public string CUSTFIELDTYPE { get; set; }
        public string CODETYPE { get; set; }
        public string MINVALUE { get; set; }
        public string MAXVALUE { get; set; }
        public string ISVISIBLE { get; set; }
        public string ISREQUIRED { get; set; }
        public string DEFAULTVALUE { get; set; }
        public string USECODEDESC { get; set; }
        public string LINKFIELDNAME { get; set; }
        public string FORCESORTBYCODE { get; set; }
        public int SEQUENCEID { get; set; }
    }
}
