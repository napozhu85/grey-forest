﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using static System.Console;

namespace AVRInterface
{
    internal class Program
    {
        public static Cityworks _db = new Cityworks();
        public static string[] _fields;

        private static void Main(string[] args)
        {
            if (!args.Any())
            {
                WriteLine("Please specify the input argument.");
                return;
            }

            WriteLine("AVR / Cityworks Interface");
            string path, prefix, extension;
            try
            {
                switch (args[0])
                {
                    case "FIXCOORD":
                        byte[] data;
                        var wos = _db.WORKORDERs.Where(w => w.WOXCOORDINATE == 0 && w.WOYCOORDINATE == 0 && w.WOADDRESS != null && w.WOADDRESS != "").ToArray();
                        foreach (var wo in wos)
                        {
                            var geoCode = $"{Properties.Settings.Default.GeocodeService}/findAddressCandidates?Single+Line+Input={wo.WOADDRESS}&f=pjson";
                            using (var client = new WebClient())
                            {
                                data = client.DownloadData(geoCode);
                            }

                            var json = Encoding.UTF8.GetString(data);
                            var jObj = JObject.Parse(json);
                            var candidate = jObj["candidates"]?.FirstOrDefault();
                            if (candidate != null)
                            {
                                var geometry = candidate["location"];
                                wo.WOXCOORDINATE = (decimal)geometry["x"];
                                wo.WOYCOORDINATE = (decimal)geometry["y"];
                            }
                            _db.SaveChanges();
                            WriteLine($"WO#{wo.WORKORDERID} updated");
                        }
                        break;

                    case "FIXNAME":
                        foreach (var wo in _db.WORKORDERs.ToArray())
                        {
                            if (wo.REQUESTEDBY.IndexOf(',') < 0) wo.REQUESTEDBY = GetName(wo.REQUESTEDBY);
                            if (wo.SUBMITTO.IndexOf(',') < 0) wo.SUBMITTO = GetName(wo.SUBMITTO);
                            _db.SaveChanges();
                            WriteLine($"WO#{wo.WORKORDERID} updated");
                        }
                        break;

                    case "IMPORT":
                        (path, prefix, extension) = GetPath(Properties.Settings.Default.ImportFileLocation);
                        Directory.GetFiles(path, $"{prefix}*{extension}").ToList()
                            .ForEach(fileName =>
                            {
                                WriteLine($"Processing: {fileName}");
                                using (var reader = new StreamReader(fileName))
                                {
                                    while (!reader.EndOfStream)
                                    {
                                        var combinedFields = new List<string>();
                                        var initialFields = reader.ReadLine().Split(',').ToList();
                                        for (var i = 0; i < initialFields.Count - 1; ++i)
                                        {
                                            var j = i;
                                            if (initialFields[i].StartsWith("\"") && !initialFields[i].EndsWith("\""))
                                            {
                                                do
                                                {
                                                    j++;
                                                    initialFields[i] += ',' + initialFields[j];
                                                }
                                                while (!initialFields[j].EndsWith("\""));
                                            }
                                            combinedFields.Add(initialFields[i]);
                                            i = j;
                                        }
                                        _fields = combinedFields.Select(f => f.StartsWith("\"") ? (f.Length < 2 ? "" : f.Substring(1, f.Length - 2)) : f).ToArray();

                                        var flag = false;
                                        long woSid = 0;
                                        var woNo = _fields[3].Trim();
                                        var wo = _db.WORKORDERs.FirstOrDefault(w => w.TEXT1 == woNo);
                                        if (wo is null)
                                        {
                                            flag = true;
                                            var serviceCode = _fields[11].Trim();
                                            var woTemplate = _db.AVRWOTEMPLATEMAPPINGs.FirstOrDefault(m => m.SERVICECODE == serviceCode);
                                            if (woTemplate is null) continue;

                                            woSid = CreateWorkOrder(woTemplate.WOTEMPLATEID, _fields[0].Trim().Replace("-", "").Substring(0, 8), _fields[2].Trim());
                                            wo = _db.WORKORDERs.FirstOrDefault(w => w.WORKORDERSID == woSid);
                                            wo.TEXT1 = woNo;
                                            if (!_db.RECENTACTIVITies.Any(r => r.KIND == "WORKORDER" && r.ACTIVITYSID == woSid))
                                            {
                                                var pwSysId = _db.PWSYSIDs.FirstOrDefault(p => p.SYSTYPE == "RECENTACTIVITY");
                                                if (pwSysId != null)
                                                {
                                                    _db.Entry(pwSysId).Reload();
                                                    var activityId = (int)pwSysId.ID;
                                                    _db.PwSysId_SetId("RECENTACTIVITY", activityId + 1);
                                                    _db.RECENTACTIVITies.Add(new RECENTACTIVITY
                                                    {
                                                        ID = activityId,
                                                        KIND = "WORKORDER",
                                                        EMPLOYEESID = 1,
                                                        DATETIMESTAMP = DateTime.Now,
                                                        ACTIVITYID = wo.WORKORDERID,
                                                        ACTIVITYSID = woSid,
                                                        DISPLAYID = wo.WORKORDERID,
                                                        DESCRIPTION = wo.DESCRIPTION
                                                    });
                                                }
                                            }
                                            _db.SaveChanges();

                                            WriteLine($"AVR WO#{woNo} / Cityworks WO#{woSid} created");
                                        }
                                        else
                                        {
                                            woSid = wo.WORKORDERSID;
                                            WriteLine($"AVR WO#{woNo} / Cityworks WO#{woSid} updated");
                                        }

                                        for (var i = 0; i < _fields.Length; ++i)
                                        {
                                            var fieldValue = _fields[i].Trim();
                                            if (fieldValue == "" && i != 5 && i != 7 && i != 32 && i != 36 && (i < 39 || i > 43)) continue;

                                            switch (i)
                                            {
                                                case 2:
                                                    break;

                                                case 4:
                                                    wo.INITIATEDATE = GetDate(fieldValue);
                                                    break;

                                                case 5:
                                                    if (wo.INITIATEDATE.HasValue)
                                                    {
                                                        wo.INITIATEDATE = wo.INITIATEDATE.Value.Date + GetTime(fieldValue);
                                                    }
                                                    break;

                                                case 6:
                                                    wo.PROJSTARTDATE = GetDate2(fieldValue);
                                                    break;

                                                case 7:
                                                    if (wo.PROJSTARTDATE.HasValue)
                                                    {
                                                        wo.PROJSTARTDATE = wo.PROJSTARTDATE.Value.Date + GetTime2(fieldValue);
                                                    }
                                                    break;

                                                case 8:
                                                    wo.REQUESTEDBY = GetName(fieldValue);
                                                    break;

                                                case 9:
                                                    wo.SUBMITTO = GetName(fieldValue);
                                                    break;

                                                case 12:
                                                    wo.TEXT3 = fieldValue;
                                                    break;

                                                case 13:
                                                    switch (fieldValue)
                                                    {
                                                        case "HIGH":
                                                            wo.PRIORITY = "1";
                                                            break;

                                                        case "MEDIUM":
                                                            wo.PRIORITY = "3";
                                                            break;

                                                        case "SCHEDULED":
                                                            wo.PRIORITY = "5";
                                                            break;
                                                    }
                                                    break;

                                                case 14:
                                                    wo.TEXT2 = fieldValue;
                                                    if (fieldValue == "VOID") wo.STATUS = "CANCEL";
                                                    break;

                                                case 16:
                                                    wo.ACTUALFINISHDATE = GetDate2(fieldValue);
                                                    break;

                                                case 23:
                                                    var district = fieldValue;
                                                    var code = _db.PWCODEs.FirstOrDefault(p => p.CODETYPE == "ADISTRCT" && p.DESCRIPTION == district);
                                                    if (!(code is null)) wo.DISTRICT = code.CODE;
                                                    break;

                                                case 28:
                                                case 58:
                                                case 82:
                                                    var tag = GetTag(i);
                                                    if (!fieldValue.StartsWith(tag)) fieldValue = tag + fieldValue;

                                                    if (!_db.WORKORDERCOMMENTs.Any(c => c.WORKORDERSID == woSid && c.COMMENTS == fieldValue))
                                                    {
                                                        var pwSysId = _db.PWSYSIDs.FirstOrDefault(p => p.SYSTYPE == "COMMENT");
                                                        if (pwSysId != null)
                                                        {
                                                            _db.Entry(pwSysId).Reload();
                                                            var commentId = (int)pwSysId.ID;
                                                            _db.PwSysId_SetId("COMMENT", commentId + 1);
                                                            _db.WORKORDERCOMMENTs.Add(new WORKORDERCOMMENT
                                                            {
                                                                COMMENTID = commentId,
                                                                WORKORDERID = wo.WORKORDERID,
                                                                WORKORDERSID = woSid,
                                                                AUTHORSID = 1,
                                                                COMMENTS = fieldValue.Replace(',', ' '),
                                                                DATECREATED = DateTime.Now,
                                                                LASTMODIFIED = DateTime.Now,
                                                                LASTMODIFIEDBYSID = 1
                                                            });
                                                        }
                                                    }
                                                    break;

                                                default:
                                                    UpdateCustField(woSid, i);
                                                    break;
                                            }
                                        }
                                        _db.SaveChanges();

                                        if (flag)
                                        {
                                            _db.UPDATED_WORKORDER.RemoveRange(_db.UPDATED_WORKORDER.Where(w => w.WORKORDERID == wo.WORKORDERID));
                                            _db.SaveChanges();
                                        }
                                    }
                                }
                                File.Move(fileName, $"{Properties.Settings.Default.ImportSaveLocation}{Path.GetFileName(fileName)}");
                            });
                        break;

                    case "EXPORT":
                        (path, prefix, extension) = GetPath(Properties.Settings.Default.ExportFileLocation);
                        Directory.GetFiles(path, $"{prefix}*.wrk").ToList()
                            .ForEach(f =>
                            {
                                File.Move(f, $"{Properties.Settings.Default.ExportSaveLocation}{Path.GetFileName(f)}");
                            });

                        var workOrders = _db.WORKORDERs.Where(w => w.TEXT1 != "")
                            .Join(_db.UPDATED_WORKORDER,
                            w => w.WORKORDERID, u => u.WORKORDERID,
                            (w, u) => w).Distinct().ToArray();
                        _db.UPDATED_WORKORDER.RemoveRange(_db.UPDATED_WORKORDER);
                        _db.SaveChanges();

                        foreach (var wo in workOrders)
                        {
                            var woSid = wo.WORKORDERSID;
                            var avrWoId = wo.TEXT1;
                            var fileName = $"{path}\\{prefix}{avrWoId.PadLeft(9, '0')}_{DateTime.Now:MMddyyHHmmss}{extension}";
                            WriteLine($"Processing: {fileName}");
                            WriteLine($"AVR WO#{avrWoId} / Cityworks WO#{woSid} exported");
                            using (var writer = new StreamWriter(fileName))
                            {
                                for (var i = 0; i <= 82; ++i)
                                {
                                    switch (i)
                                    {
                                        case 2:
                                            writer.WriteCell(wo.WOADDRESS);
                                            break;

                                        case 3:
                                            writer.WriteCell(avrWoId);
                                            break;

                                        case 4:
                                            writer.WriteCell(wo.INITIATEDATE.HasValue ? wo.INITIATEDATE.Value.ToString("MM/dd/yy") : "");
                                            break;

                                        case 5:
                                            writer.WriteCell(wo.INITIATEDATE.HasValue ? wo.INITIATEDATE.Value.ToString("HHmmtt", CultureInfo.InvariantCulture) : "");
                                            break;

                                        case 6:
                                            writer.WriteCell(wo.PROJSTARTDATE.HasValue ? wo.PROJSTARTDATE.Value.ToString("MM/dd/yyyy") : "");
                                            break;

                                        case 7:
                                            writer.WriteCell(wo.PROJSTARTDATE.HasValue ? wo.PROJSTARTDATE.Value.ToString("HH:mm tt", CultureInfo.InvariantCulture) : "");
                                            break;

                                        case 8:
                                            writer.WriteCell(GetName(wo.REQUESTEDBY));
                                            break;

                                        case 9:
                                            writer.WriteCell(GetName(wo.SUBMITTO));
                                            break;

                                        case 11:
                                            var woTemplate = _db.AVRWOTEMPLATEMAPPINGs.FirstOrDefault(m => m.WOTEMPLATEID == wo.WOTEMPLATEID);
                                            writer.WriteCell(woTemplate?.SERVICECODE);
                                            break;

                                        case 12:
                                            writer.WriteCell(wo.TEXT3);
                                            break;

                                        case 13:
                                            switch (wo.PRIORITY)
                                            {
                                                case "1":
                                                    writer.WriteCell("HIGH");
                                                    break;

                                                case "3":
                                                    writer.WriteCell("MEDIUM");
                                                    break;

                                                case "5":
                                                    writer.WriteCell("SCHEDULED");
                                                    break;

                                                default:
                                                    writer.WriteCell();
                                                    break;
                                            }
                                            break;

                                        case 14:
                                            if (wo.STATUS == "CANCEL")
                                            {
                                                writer.WriteCell("VOID");
                                            }
                                            else if (!string.IsNullOrWhiteSpace(wo.TEXT2))
                                            {
                                                writer.WriteCell(wo.TEXT2);
                                            }
                                            else
                                            {
                                                writer.WriteCell("OPEN");
                                            }
                                            break;

                                        case 16:
                                            writer.WriteCell(wo.ACTUALFINISHDATE.HasValue ? wo.ACTUALFINISHDATE.Value.ToString("MM/dd/yyyy") : "");
                                            break;

                                        case 25:
                                            var code = _db.PWCODEs.FirstOrDefault(p => p.CODETYPE == "ADISTRCT" && p.CODE == wo.DISTRICT);
                                            writer.WriteCell(code?.CODE);
                                            break;

                                        case 23:
                                            code = _db.PWCODEs.FirstOrDefault(p => p.CODETYPE == "ADISTRCT" && p.CODE == wo.DISTRICT);
                                            writer.WriteCell(code?.DESCRIPTION.ToUpper());
                                            break;

                                        case 28:
                                        case 58:
                                        case 82:
                                            GetComments(writer, woSid, i);
                                            break;

                                        default:
                                            writer.WriteCell(GetCustField(woSid, i), i == 82);
                                            break;
                                    }
                                }
                            }
                        }
                        break;
                }
            }
            catch (DbEntityValidationException e)
            {
                using (var file = GetLogger())
                {
                    using (var writer = new StreamWriter(file))
                    {
                        writer.WriteLine(DateTime.Now);
                        writer.WriteLine($"Error: {e.Message}");
                        writer.WriteLine($"Details: ");
                        foreach (var error in e.EntityValidationErrors)
                        {
                            WriteLine($"{error.Entry.Entity.GetType().Name}: {error.Entry.State}");
                            foreach (var ve in error.ValidationErrors)
                            {
                                WriteLine($"{ve.PropertyName}: {ve.ErrorMessage}");
                            }
                        }
                        WriteLine(e.StackTrace);
                        writer.WriteLine();
                    }
                }
            }
            catch (Exception e)
            {
                using (var file = GetLogger())
                {
                    using (var writer = new StreamWriter(file))
                    {
                        writer.WriteLine(DateTime.Now);
                        writer.WriteLine($"Error: {e.Message}");
                        writer.WriteLine($"Details: {e.InnerException?.Message}");
                        writer.WriteLine($"{e.InnerException?.InnerException?.Message}");
                        writer.WriteLine(e.StackTrace);
                        writer.WriteLine();
                    }
                }
            }
        }

        private static long CreateWorkOrder(string woTemplateId, string accountNo, string woAddress)
        {
            long woSid = 0;
            var woId = "";
            var woTemplate = _db.WOTEMPLATEs.First(t => t.WOTEMPLATEID == woTemplateId);
            var featureId = 0M;
            var x = 0M;
            var y = 0M;
            var location = "";

            var flag = false;
            Exception exception = null;
            for (var i = 0; i < 5; ++i)
            {
                if (flag) break;

                try
                {
                    var servicePoint = $"{Properties.Settings.Default.AssetService}/query?text={accountNo}&outFields=*&returnGeometry=true&f=pjson";
                    byte[] data;
                    using (var client = new WebClient())
                    {
                        data = client.DownloadData(servicePoint);
                    }

                    var json = Encoding.UTF8.GetString(data);
                    var jObj = JObject.Parse(json);
                    var feature = jObj["features"]?.FirstOrDefault();
                    if (feature != null)
                    {
                        featureId = (decimal)feature["attributes"]["OBJECTID"];
                        location = feature["attributes"]["Location"].ToString();

                        var geometry = feature["geometry"];
                        x = (decimal)geometry["x"];
                        y = (decimal)geometry["y"];
                    }
                    else
                    {
                        var geoCode = $"{Properties.Settings.Default.GeocodeService}/findAddressCandidates?Single+Line+Input={woAddress}&f=pjson";
                        using (var client = new WebClient())
                        {
                            data = client.DownloadData(geoCode);
                        }

                        json = Encoding.UTF8.GetString(data);
                        jObj = JObject.Parse(json);
                        var candidate = jObj["candidates"]?.FirstOrDefault();
                        if (candidate != null)
                        {
                            var geometry = candidate["location"];
                            x = (decimal)geometry["x"];
                            y = (decimal)geometry["y"];
                        }
                    }
                }
                catch
                {
                }
                if (string.IsNullOrWhiteSpace(location)) location = woAddress;

                WORKORDER wo = null;
                try
                {
                    var pwSysId = _db.PWSYSIDs.FirstOrDefault(p => p.SYSTYPE == "WORKORDER");
                    if (pwSysId != null)
                    {
                        woSid = (long)pwSysId.ID;
                        woId = woSid.ToString();
                        _db.PwSysId_SetId("WORKORDER", woSid + 1);

                        wo = new WORKORDER
                        {
                            WORKORDERID = woId,
                            WORKORDERSID = woSid,
                            WOTEMPLATEID = woTemplate.WOTEMPLATEID,
                            DESCRIPTION = woTemplate.DESCRIPTION,
                            STATUS = "OPEN",
                            APPLYTOENTITY = woTemplate.APPLYTOENTITY,
                            PRIORITY = woTemplate.PRIORITY,
                            NUMDAYSBEFORE = woTemplate.NUMDAYSBEFORE,
                            MAPTEMPLATENAME = woTemplate.MAPTEMPLATENAME,
                            WOMAPEXTENT = "A",
                            WOMAPSCALE = woTemplate.WOMAPSCALE,
                            WOOUTPUT = woTemplate.WOOUTPUT,
                            WOCATEGORY = woTemplate.WOCATEGORY,
                            ACCTNUM = woTemplate.ACCTNUM,
                            SHOP = woTemplate.SHOP,
                            CYCLETYPE = woTemplate.CYCLETYPE,
                            CYCLEINTERVALNUM = woTemplate.CYCLEINTERVALNUM,
                            CYCLEINTERVALUNIT = woTemplate.CYCLEINTERVALUNIT,
                            CYCLEFROM = woTemplate.CYCLEFROM,
                            DOMAINID = woTemplate.DOMAINID,
                            WOCUSTFIELDCATID = woTemplate.WOCUSTFIELDCATID,
                            CANCEL = woTemplate.CANCEL,
                            STAGE = woTemplate.STAGE,
                            EXPENSETYPE = woTemplate.EXPENSETYPE,
                            UNITSACCOMPDESC = woTemplate.UNITSACCOMPDESC,
                            UNITSACCOMPDESCLOCK = woTemplate.UNITSACCOMPDESCLOCK,
                            ISREACTIVE = woTemplate.ISREACTIVE,
                            EFFORT = woTemplate.EFFORT,
                            INITIATEDBYAPP = "INTERNAL",
                            SUPERVISOR = "",
                            REQUESTEDBY = "",
                            INITIATEDBY = "AVR, AVR",
                            PROJECTNAME = "",
                            LOCATION = "",
                            WOCLOSEDBY = "",
                            SOURCEWOID = "",
                            UNATTACHED = "Y",
                            WOCOST = 0,
                            WOLABORCOST = 0,
                            WOMATCOST = 0,
                            WOEQUIPCOST = 0,
                            WOPERMITCOST = 0,
                            SUBMITTO = "",
                            SUBMITTOOPENBY = "",
                            WORKCOMPLETEDBY = "",
                            MAPPAGE = "",
                            LEGALBILLABLE = "N",
                            CONTRBILLABLE = "N",
                            CANCELLEDBY = "",
                            UPDATEMAP = "Y",
                            TILENO = "",
                            WOADDRESS = woAddress,
                            CREATEDBYCYCLE = "N",
                            TRANSTOWOID = "",
                            LINEITEMCOST = 0,
                            CANCELREASON = "",
                            TEXT1 = "",
                            TEXT2 = "",
                            TEXT3 = "",
                            TEXT4 = "",
                            TEXT5 = "",
                            TEXT6 = "",
                            TEXT7 = "",
                            TEXT8 = "",
                            TEXT9 = "",
                            TEXT10 = "",
                            TEXT11 = "",
                            TEXT12 = "",
                            TEXT13 = "",
                            TEXT14 = "",
                            TEXT15 = "",
                            TEXT16 = "",
                            TEXT17 = "",
                            TEXT18 = "",
                            TEXT19 = "",
                            TEXT20 = "",
                            RESOLUTION = "",
                            CONTRACTWOID = "",
                            STREETNAME = "",
                            SUPERVISORSID = 0,
                            REQUESTEDBYSID = 0,
                            INITIATEDBYSID = 31,
                            SUBMITTOSID = 0,
                            SUBMITTOOPENBYSID = 0,
                            WORKCOMPLETEDBYSID = 0,
                            CANCELLEDBYSID = 0,
                            CLOSEDBYSID = 0,
                            WOXCOORDINATE = x,
                            WOYCOORDINATE = y
                        };
                        _db.WORKORDERs.Add(wo);
                        _db.SaveChanges();
                        _db.Entry(pwSysId).State = EntityState.Detached;
                        flag = true;
                    }
                    else
                    {
                        return woSid;
                    }
                }
                catch (Exception e)
                {
                    exception = e;
                    if (!(wo is null)) _db.WORKORDERs.Remove(wo);
                }
            }
            if (!flag) throw exception;

            flag = false;
            exception = null;
            for (var i = 0; i < 5; ++i)
            {
                if (flag) break;

                WORKORDERENTITY woEntity = null;
                try
                {
                    var pwSysId2 = _db.PWSYSIDs.FirstOrDefault(p => p.SYSTYPE == "WORKORDERENTITY");
                    if (pwSysId2 != null)
                    {
                        _db.PwSysId_SetId("WORKORDERENTITY", (int)pwSysId2.ID + 1);

                        woEntity = new WORKORDERENTITY
                        {
                            OBJECTID = pwSysId2.ID,
                            WORKORDERID = woId,
                            WORKORDERSID = woSid,
                            ENTITYUID = accountNo,
                            ENTITYTYPE = "SERVICE POINT",
                            FEATURE_ID = featureId,
                            FEATURE_TYPE = "SERVICE POINT",
                            LOCATION = location,
                            LEGACYID = "",
                            ENTITYSID = featureId,
                            WORKCOMPLETED = "N",
                            FEATUREUID = accountNo,
                            X = x,
                            Y = y
                        };
                        _db.WORKORDERENTITies.Add(woEntity);
                        _db.SaveChanges();
                        _db.Entry(pwSysId2).State = EntityState.Detached;
                        flag = true;
                    }
                    else
                    {
                        return woSid;
                    }
                }
                catch (Exception e)
                {
                    exception = e;
                    if (!(woEntity is null)) _db.WORKORDERENTITies.Remove(woEntity);
                }
            }
            if (!flag) throw exception;

            _db.WORKORDERs.Where(w => w.WORKORDERSID == woSid)
                .Join(_db.WOTEMPLATEs,
                o => o.WOTEMPLATEID, t => t.WOTEMPLATEID,
                (o, t) => t.WOCUSTFIELDCATID)
                .Join(_db.CATEGORYCUSTFIELDs,
                i => i, c => c.CATEGORYID,
                (i, c) => c).Distinct().ToList()
                .ForEach(c => _db.WOCUSTFIELDs.Add(new WOCUSTFIELD
                {
                    WORKORDERID = woId,
                    WORKORDERSID = woSid,
                    CUSTFIELDID = c.CUSTFIELDID,
                    CUSTFIELDNAME = c.CUSTFIELDNAME,
                    CUSTFIELDVALUE = ""
                }));
            _db.SaveChanges();
            return woSid;
        }

        private static void GetComments(StreamWriter writer, long woSid, int index)
        {
            var tag = GetTag(index);
            var comments = (index == 82 ? _db.WORKORDERCOMMENTs.Where(c => c.WORKORDERSID == woSid && !c.COMMENTS.StartsWith("[METER NOTE]")
                && !c.COMMENTS.StartsWith("[COMMENT]") && !c.COMMENTS.StartsWith("[PROBLEMS ENCOUNTERED]"))
                : _db.WORKORDERCOMMENTs.Where(c => c.WORKORDERSID == woSid && c.COMMENTS.StartsWith(tag)))
                .Where(c => c.COMMENTS != null && c.COMMENTS != "")
                .Select(c => c.COMMENTS).ToArray();
            for (var i = 0; i < comments.Length; ++i)
            {
                comments[i] = comments[i].Replace(',', ' ');
                if (index == 58) comments[i] = comments[i].Replace('.', '|');
            }
            var content = comments.Any() ? comments.Aggregate((a, b)
                => $"{(a.StartsWith(tag) ? a.Substring(tag.Length) : a)}{(index == 58 ? '|' : ' ')}{(b.StartsWith(tag) ? b.Substring(tag.Length) : b)}") : null;
            writer.WriteCell(content?.Trim());
        }

        private static string GetCustField(long woSid, int index)
        {
            var fields = GetCustFields(woSid, index == 37 || index == 38 ? 36 : index);
            if (!fields.Any()) return null;

            if (fields.Count() > 1)
            {
                if (index == 61)
                {
                    var yes = fields.FirstOrDefault(f => f.CUSTFIELDVALUE == "Yes");
                    return yes?.CUSTFIELDNAME.Substring(8).ToUpper();
                }
            }

            var field = fields.First();
            var codeType = _db.CATEGORYCUSTFIELDs.First(c => c.CUSTFIELDID == field.CUSTFIELDID).CODETYPE;
            if (codeType == "BOOLEAN" || codeType == "LEAK ON" || codeType == "MTRSTS")
            {
                switch (field.CUSTFIELDVALUE)
                {
                    case "Yes":
                        return "YES";

                    case "No":
                        return "NO";

                    case "GFU System":
                    case "GFU System and Customer":
                        return "GFU";

                    case "Customer":
                        return "Customer";

                    case "Other":
                        return "OTHER";

                    case "Inventory":
                        return "A";

                    case "Field":
                        return "B";

                    case "Repair":
                        return "C";

                    case "Scrapped":
                        return "S";
                }
            }
            else
            {
                switch (index)
                {
                    case 32:
                        return field.CUSTFIELDVALUE == "No Leaks at this Time" ? "X" : null;

                    case 33:
                        return field.CUSTFIELDVALUE == "Leak Found Meter Left Off" ? "X" : null;

                    case 36:
                        return field.CUSTFIELDVALUE == "On" ? "YES" : null;

                    case 37:
                        return field.CUSTFIELDVALUE == "Off" ? "YES" : null;

                    case 38:
                        return field.CUSTFIELDVALUE == "Off and Barrel Locked" ? "YES" : null;

                    case var n when (n >= 39 && n < 44):
                        return field.CUSTFIELDVALUE == "Pilot Light" ? "YES" : null;

                    case var n when (n >= 44 && n < 49):
                        return field.CUSTFIELDVALUE == "Left Off" ? "YES" : null;

                    case var n when (n >= 49 && n < 54):
                        return field.CUSTFIELDVALUE == "Red Tag" ? "YES" : null;

                    case 73:
                    case 76:
                    case 81:
                        return field.CUSTFIELDVALUE.Replace('.', '|');

                    default:
                        return field.CUSTFIELDVALUE;
                }
            }

            return null;
        }

        private static List<WOCUSTFIELD> GetCustFields(long woSid, int id)
        {
            return _db.WOCUSTFIELDs.Where(f => f.WORKORDERSID == woSid)
                .Join(_db.WORKORDERs,
                f => f.WORKORDERSID, w => w.WORKORDERSID,
                (f, w) => new { f, w.WOTEMPLATEID })
                .Join(_db.WOTEMPLATEs,
                x => x.WOTEMPLATEID, t => t.WOTEMPLATEID,
                (x, t) => new { x.f, t.WOCUSTFIELDCATID })
                .Join(_db.CATEGORYCUSTFIELDs,
                x => x.WOCUSTFIELDCATID, c => c.CATEGORYID,
                (x, c) => new { x.f, c.CUSTFIELDID })
                .Join(_db.AVRCUSTFIELDMAPPINGs.Where(m => m.AVRFIELDID == id),
                x => new { x.CUSTFIELDID, id = x.f.CUSTFIELDID }, m => new { m.CUSTFIELDID, id = m.CUSTFIELDID },
                (x, m) => x.f).ToList();
        }

        private static DateTime? GetDate(string date) => DateTime.TryParseExact(date, "MM/dd/yy", CultureInfo.InvariantCulture, DateTimeStyles.None, out var convertedDate)
            ? convertedDate : (DateTime?)null;

        private static DateTime? GetDate2(string date) => DateTime.TryParseExact(date, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out var convertedDate)
            ? convertedDate : (DateTime?)null;

        private static FileStream GetLogger() => File.Open($"{AppDomain.CurrentDomain.BaseDirectory}\\Log.txt", FileMode.Append);

        private static string GetName(string name)
        {
            var hasComma = name.IndexOf(',') > 0;
            var names = name.Split(new[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);
            switch (names.Length)
            {
                case 0:
                    return "";

                case 1:
                    return names[0];

                case 2:
                    return hasComma ? $"{names[1]} {names[0]}" : $"{names[1]}, {names[0]}";

                case 3:
                    return hasComma ? $"{names[1]} {names[2]} {names[0]}" : $"{names[2]}, {names[0]} {names[1]}";

                case 4:
                    return hasComma ? $"{names[1]} {names[2]} {names[0]} {names[3]}" : $"{names[2]}, {names[0]} {names[1]} {names[3]}";

                default:
                    return name;
            }
        }

        private static (string, string, string) GetPath(string location)
        {
            var position1 = location.LastIndexOf('.');
            var position2 = location.LastIndexOf('\\');
            var prefix = location.Substring(position2 + 1, position1 - position2 - 1);
            var suffix = location.Substring(position1);
            var path = location.Substring(0, position2);
            return (path, prefix, suffix);
        }

        private static string GetTag(int index)
        {
            switch (index)
            {
                case 28:
                    return "[METER NOTE]";

                case 58:
                    return "[PROBLEMS ENCOUNTERED]";

                case 82:
                    return "[COMMENT]";

                default:
                    return "";
            }
        }

        private static TimeSpan GetTime(string time) => (DateTime.TryParseExact(time.Substring(0, 4), "HHmm", CultureInfo.InvariantCulture, DateTimeStyles.None,
            out var convertedTime) ? convertedTime : (DateTime?)null)?.TimeOfDay ?? new TimeSpan(0, 1, 0);

        private static TimeSpan GetTime2(string time) => (DateTime.TryParse(time, out var convertedTime) ? convertedTime : (DateTime?)null)?.TimeOfDay
            ?? new TimeSpan(0, 1, 0);

        private static string SetMaxlen(string str, int length) => str?.Substring(0, Math.Min(str.Length, length));

        private static void UpdateCustField(long woSid, int index)
        {
            var fields = GetCustFields(woSid, index);
            if (!fields.Any()) return;

            if (fields.Count() > 1)
            {
                if (index == 61)
                {
                    fields.ForEach(f => f.CUSTFIELDVALUE = f.CUSTFIELDNAME == $"Leak At {_fields[index]}" ? "Yes" : "No");
                }
            }

            var field = fields.First();
            var codeType = _db.CATEGORYCUSTFIELDs.First(c => c.CUSTFIELDID == field.CUSTFIELDID)?.CODETYPE;
            if (codeType == "BOOLEAN" || codeType == "LEAK ON" || codeType == "MTRSTS")
            {
                switch (_fields[index])
                {
                    case "YES":
                    case "yes":
                        field.CUSTFIELDVALUE = "Yes";
                        break;

                    case "NO":
                    case "no":
                        field.CUSTFIELDVALUE = "No";
                        break;

                    case "GFU":
                        field.CUSTFIELDVALUE = "GFU System";
                        break;

                    case "Customer":
                        field.CUSTFIELDVALUE = "Customer";
                        break;

                    case "OTHER":
                        field.CUSTFIELDVALUE = "Other";
                        break;

                    case "A":
                        field.CUSTFIELDVALUE = "Inventory";
                        break;

                    case "B":
                        field.CUSTFIELDVALUE = "Field";
                        break;

                    case "C":
                        field.CUSTFIELDVALUE = "Repair";
                        break;

                    case "S":
                        field.CUSTFIELDVALUE = "Scrapped";
                        break;
                }
            }
            else
            {
                if (index == 32)
                {
                    if (_fields[index] == "X") field.CUSTFIELDVALUE = "No Leaks at this Time";
                    else if (_fields[index + 1] == "X") field.CUSTFIELDVALUE = "Leak Found Meter Left Off";
                }
                else if (index == 36)
                {
                    if (_fields[index] == "YES") field.CUSTFIELDVALUE = "On";
                    else if (_fields[index + 1] == "YES") field.CUSTFIELDVALUE = "Off";
                    else if (_fields[index + 2] == "YES") field.CUSTFIELDVALUE = "Off and Barrel Locked";
                }
                else if (index >= 39 && index <= 43)
                {
                    if (_fields[index] == "YES") field.CUSTFIELDVALUE = "Pilot Light";
                    else if (_fields[index + 5] == "YES") field.CUSTFIELDVALUE = "Left Off";
                    else if (_fields[index + 10] == "YES") field.CUSTFIELDVALUE = "Red Tag";
                }
                else
                {
                    field.CUSTFIELDVALUE = SetMaxlen(_fields[index], 250);
                }
            }
        }
    }

    public static class StreamWriterExtension
    {
        public static void WriteCell(this StreamWriter writer, object content = null, bool isLast = false)
        {
            writer.Write($"\"{content}\"{(isLast ? "" : ",")}");
        }
    }
}